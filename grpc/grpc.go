package grpc

import (
	"gitlab.com/ecomm3130632/ecomm_go_order_service/config"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/genproto/user_service"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/grpc/client"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/grpc/service"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/pkg/logger"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
