package service

import (
	"context"

	"gitlab.com/ecomm3130632/ecomm_go_order_service/config"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/genproto/user_service"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/grpc/client"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/pkg/logger"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/storage"
)

type UserService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*user_service.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *UserService {
	return &UserService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *UserService) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.User, err error) {

	u.log.Info("-----------CreateUser----------", logger.Any("req", req))

	id, err := u.strg.User().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateUser ->User->Create", logger.Error(err))
		return nil, err
	}

	resp, err = u.strg.User().GetByID(ctx, &user_service.UserPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("GetByIdUser->GetById", logger.Error(err))
		return nil, err
	}

	return
}

func (u *UserService) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (*user_service.User, error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err := u.strg.User().GetByID(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->User->GetByID", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *UserService) GetList(ctx context.Context, req *user_service.GetListUserRequest) (*user_service.GetListUserResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.User().GetList(ctx, &user_service.GetListUserRequest{})
	if err != nil {
		u.log.Error("GetList ->User->GetList", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *UserService) Update(ctx context.Context, req *user_service.UpdateUser) (*user_service.User, error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err := u.strg.User().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->User->Update", logger.Error(err))
		return nil, err
	}
	resp, err := u.strg.User().GetByID(ctx, &user_service.UserPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->User->GetByID", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *UserService) Delete(ctx context.Context, req *user_service.UserPrimaryKey) (*user_service.Empty, error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err := u.strg.User().Delete(ctx, &user_service.UserPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->User->Delete", logger.Error(err))
		return nil, err
	}

	return nil, nil
}
