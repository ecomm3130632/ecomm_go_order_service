package storage

import (
	"context"

	app "gitlab.com/ecomm3130632/ecomm_go_order_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
}

type UserRepoI interface {
	Create(context.Context, *app.CreateUser) (string, error)
	GetByID(context.Context, *app.UserPrimaryKey) (*app.User, error)
	GetList(context.Context, *app.GetListUserRequest) (*app.GetListUserResponse, error)
	Update(context.Context, *app.UpdateUser) (int64, error)
	Delete(context.Context, *app.UserPrimaryKey) error
}
