package postgres

import (
	"context"
	"database/sql"
	"fmt"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ecomm3130632/ecomm_go_order_service/pkg/helper"

	app "gitlab.com/ecomm3130632/ecomm_go_order_service/genproto/user_service"
)

type userRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *userRepo {
	return &userRepo{
		db: db,
	}
}

func (r *userRepo) Create(ctx context.Context, req *app.CreateUser) (string, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "users"(id, name, phone_number)
		VALUES ($1, $2, $3)
	`

	_, err := r.db.Exec(ctx, query,
		&id,
		&req.Name,
		&req.PhoneNumber,
	)

	if err != nil {
		return "", err
	}

	return id, nil
}

func (r *userRepo) GetByID(ctx context.Context, req *app.UserPrimaryKey) (*app.User, error) {
	var (
		query string

		id          sql.NullString
		name        sql.NullString
		phoneNumber sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			phone_number
		FROM users
		WHERE id = $1
	`
	fmt.Println(req.Id)
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&phoneNumber,
	)
	fmt.Println("Hello")

	if err != nil {
		return nil, err
	}

	return &app.User{
		Id:          id.String,
		Name:        name.String,
		PhoneNumber: phoneNumber.String,
	}, nil
}

func (r *userRepo) GetList(ctx context.Context, req *app.GetListUserRequest) (*app.GetListUserResponse, error) {

	var (
		resp   = &app.GetListUserResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			phone_number
		FROM "users"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			phoneNumber sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&phoneNumber,
		)

		if err != nil {
			return nil, err
		}

		resp.Users = append(resp.Users, &app.User{
			Id:          id.String,
			Name:        name.String,
			PhoneNumber: phoneNumber.String,
		})
	}

	return resp, nil
}

func (r *userRepo) Update(ctx context.Context, req *app.UpdateUser) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"users"
		SET
			name = :name,
			phone_number = :phone_number
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *userRepo) Delete(ctx context.Context, req *app.UserPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM users WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
